        </div>
        <footer class="site-footer">
            <div class="site-logo">
                <?php the_custom_logo(); ?>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <?php
                    wp_nav_menu([
                        'theme_location' => 'menu-primary',
                        'menu_id'        => 'primary-menu',
                    ]);
                    ?>
            </nav>
            <div class="social-links">
                <a href="#">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="#">
                    <i class="fab fa-slack-hash"></i>
                </a>
                <a href="#">
                    <i class="fab fa-github-square"></i>
                </a>
            </div>
            <div class="copyright">
                Copyright© Arifur Rahman Tushar 2019. All rights reserved
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
