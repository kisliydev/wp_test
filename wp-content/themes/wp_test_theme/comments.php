<?php
    if (post_password_required()) {
        return;
    }
?>

<div id="comments" class="comments-area">
    <?php
        if (have_comments()) :
    ?>
            <h2 class="comments-title">
                <?php
                $wp_test_theme_comment_count = get_comments_number();
                if ($wp_test_theme_comment_count === '1') {
                    printf(
                            esc_html__('One thought on &ldquo;%1$s&rdquo;', 'wp_test_theme'),
                            '<span>' . wp_kses_post(get_the_title()) . '</span>'
                    );
                } else {
                    printf(
                            esc_html(_nx('%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $wp_test_theme_comment_count, 'comments title', 'wp_test_theme')), number_format_i18n($wp_test_theme_comment_count),
                            '<span>' . wp_kses_post(get_the_title()) . '</span>'
                    );
                }
                ?>
            </h2>

            <?php the_comments_navigation(); ?>

            <ol class="comment-list">
                <?php
                    wp_list_comments([
                        'style'      => 'ol',
                        'short_ping' => true,
                    ]);
                ?>
            </ol>
            <?php
                the_comments_navigation();
                if (!comments_open()) :
            ?>
                    <p class="no-comments"><?php esc_html_e('Comments are closed.', 'wp_test_theme'); ?></p>
            <?php
                endif;
        endif;

        comment_form();
    ?>
</div>
