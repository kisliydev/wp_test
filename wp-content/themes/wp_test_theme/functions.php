<?php
/**
 * wp_test_theme functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 * @package wp_test_theme
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

function wp_test_theme_setup() {
    load_theme_textdomain( 'wp_test_theme', get_template_directory() . '/languages' );

    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    register_nav_menus([
        'menu-primary' => esc_html__('Primary', 'wp_test_theme'),
    ]);

    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'style',
        'script',
    ]);

    add_theme_support('custom-background', apply_filters('wp_test_theme_custom_background_args', [
        'default-color' => 'ffffff',
        'default-image' => '',
    ]));

    add_theme_support( 'customize-selective-refresh-widgets' );

    add_theme_support('custom-logo', [
        'height'      => 250,
        'width'       => 250,
        'flex-width'  => true,
        'flex-height' => true,
    ]);
}
add_action('after_setup_theme', 'wp_test_theme_setup');

function wp_test_theme_widgets_init() {
    register_sidebar([
        'name'          => esc_html__('Sidebar', 'wp_test_theme'),
        'id'            => 'sidebar-1',
        'description'   => esc_html__('Add widgets here.', 'wp_test_theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);
}
add_action('widgets_init', 'wp_test_theme_widgets_init');

function wp_test_theme_scripts() {
    wp_enqueue_style('wp_test_theme-style', get_stylesheet_uri(), [], _S_VERSION);
    wp_style_add_data('wp_test_theme-style', 'rtl', 'replace');

    wp_enqueue_script('wp_test_theme-scripts', get_template_directory_uri() . '/assets/js/scripts.js', ['jquery'], _S_VERSION, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'wp_test_theme_scripts');

require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/customizer.php';

