<?php
    /*
    Template Name: Home page template
    */
    get_header();
?>
    <main id="primary" class="site-main home-template">
        <?php echo do_shortcode('
            [ajax_load_more 
                id="films" 
                loading_style="blue" 
                container_type="div"
                css_classes="flim-lists" 
                post_type="film" 
                posts_per_page="2" 
                placeholder="true" 
                scroll="false" 
                no_results_text="Films not found"
            ]
        '); ?>
    </main>
<?php
    get_footer();
