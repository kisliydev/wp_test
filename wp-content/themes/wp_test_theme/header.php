<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="profile" href="https://gmpg.org/xfn/11">

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php wp_body_open(); ?>
        <header class="site-header wrapper">
            <div class="site-logo">
                <?php the_custom_logo(); ?>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <?php
                    wp_nav_menu([
                        'theme_location' => 'menu-primary',
                        'menu_id'        => 'primary-menu',
                    ]);
                ?>
                <a href="#" class="button button-primary">
                    <?php _e('Try for free', 'wp_test_theme'); ?>
                </a>
            </nav>
        </header>
        <div class="site-content wrapper">