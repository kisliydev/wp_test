jQuery(function ($) {
    const $filmContentLinks = $('.film-list .film-item .film-info .content a');

    if ($filmContentLinks.length > 0) {
        $filmContentLinks.click(function (e) {
            e.preventDefault();
            window.open($(this).attr('href'), '_blank');
        });
    }
});
